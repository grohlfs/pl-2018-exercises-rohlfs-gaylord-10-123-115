
:- dynamic male/1, female/1, parent/2.

female(anne).
female(diana).
female(elizabeth).
male(andrew).
male(charles).
male(edward).
male(harry).
male(philip).
male(william).

parent(andrew, elizabeth).
parent(andrew, philip).
parent(anne, elizabeth).
parent(anne, philip).
parent(charles, elizabeth).
parent(charles, philip).
parent(edward, elizabeth).
parent(edward, philip).
parent(harry, charles).
parent(harry, diana).
parent(william, charles).
parent(william, diana).

parent(elizabeth, william).
parent(elizabeth, diana).


% ------------------------------------------------------

mother(X, M) :-	parent(X,M), female(M).

father(X, M) :-	parent(X,M), male(M).

sibling(X, Y) :- 	mother(X, M), mother(Y, M),
					father(X, F), father(Y, F),
					X \== Y.
	
brother(X, B) :- sibling(X,B), male(B).

sister(X, S) :-	sibling(X,S), female(S).

/* grandfather needs: male, parent of parent
% grandmother needs: female, parent of parent
% grandparent needs: parent of parent
% son needs: male, parent
% daughter needs: female, parent
% grandson: male, child of child or parent of parent reversed
% granddaughter: child of child or parent of parent reversed
% grandchild: child of child  or parent of parent reversed */

grandfather(X, Y) :- parent(X, A), parent(A, Y), male(Y).
grandmother(X, Y) :- parent(X, A), parent(A, Y), female(Y).
grandparent(X, Y) :- parent(X, A), parent(A, Y).

son(X, Y) :- parent(X, Y), male(X).
daughter(X, Y) :- parent(X, Y), female(X).
child(X, Y) :- parent(X, Y).

grandson(X, Y) :- parent(Y, A), parent(A, X), male(Y).
granddaughter(X, Y) :- parent(Y, A), parent(A, X), female(Y).
grandchild(X, Y) :- parent(Y, A), parent(A, X).