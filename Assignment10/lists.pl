% Even List Elements:
% An empty List should give back true
% Split the list in two same size lists with cons pairs and watch if there are more splittable elements with recursion
% add a cut criteria

evenlist([]).
evenlist([X,Y|Z]) :- evenlist(Z),!.

% Palindrome e.g. [1,2,1] = true
% Empty and anonymous list should give back true
% append inverse relations slide 37, lecture 10
% Samples Success:
%  append([1,2],L2,[1,2,3,4]). (gives L2=[3,4]).
%  append([1,B],L2,[A,2,3,4]). (gives B=2 L2=[3,4] A=1).
%  append([1,2],L2,L3).        (gives L2=L2 L3=[1,2|L2]).
%  append([1],[2,3],L3).     (gives L3=[1,2,3]).
% 

palindrome([]).
palindrome([_]).
palindrome(X) :- append([Y|Z], [Y], X), palindrome(Z).