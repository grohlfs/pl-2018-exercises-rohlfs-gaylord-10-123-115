%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Spellchecker
%
% With SWI Prolog i was not able to use the sample code like calc to make a clean parser.
% Instead i found a very useful Input Method in pure prolog. 
% This was not the task of the exercise so i can copy it without problems. http://rowa.giso.de/languages/toki-pona/dcg/
%
% To test my spellchecker. Load the file into Prolog and type: check_grammar.
% Then you can put in your sentence and press enter. Voila....
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% ------  grammar   --------

sentence --> subject_predicate.
sentence --> subject_predicate_object_likes.
sentence --> subject_predicate_object_is.

% sentence types

subject_predicate --> subject, predicate_sp.
subject_predicate_object_likes --> subject_likes, predicate_spo_likes, object.
subject_predicate_object_is --> subject_is, predicate_spo_is, object.


% form types

subject --> article(_), noun(_).
subject --> pronoun(_).
subject_likes --> article(_), noun(_).
subject_is --> pronoun(_).

predicate_spo_is --> auxiliary(is).
predicate_spo_likes --> verb(likes).
predicate_sp --> verb(sleeps).

object --> article(_), noun(_).

% ------  words   --------

article(Article) --> [Article], { member(Article, [a,the])}.
noun(Noun) --> [Noun], { member(Noun, [girl,boy])}.
pronoun(Pronoun) --> [Pronoun], { member(Pronoun, [that,this])}.
auxiliary(Auxiliary) --> [Auxiliary], { member(Auxiliary, [is])}.
verb(Verb) --> [Verb], { member(Verb, [sleeps,likes])}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For a user friendly input:
% not necessary for the exercise, but nice to have

% This syntax is pure Prolog:

check_grammar             :- read_line(CL), wordlist(WL,CL,[]), !, sentence(WL,[]).   % Read a line from the user, put it in a list of words and check it with "sentence".

read_line(WL)             :- get0(C), codelist(C,WL).                           % Read a character code and put it in a list of codes.

codelist(10,[])           :- !.                                                 % Stop to build the list after line feed (10).
codelist(13,[])           :- !.                                                 % Stop to build the list after carriage return (13).
codelist(33,[32,33,32|X]) :- get0(C2), codelist(C2,X).                          % Put spaces (32) arround the "!" (33) and continue to build the list.
codelist(44,[32,44,32|X]) :- get0(C2), codelist(C2,X).                          % Put spaces (32) arround the "," (44) and continue to build the list.
codelist(46,[32,46,32|X]) :- get0(C2), codelist(C2,X).                          % Put spaces (32) arround the "." (46) and continue to build the list.
codelist(58,[32,58,32|X]) :- get0(C2), codelist(C2,X).                          % Put spaces (32) arround the ":" (58) and continue to build the list.
codelist(59,[32,59,32|X]) :- get0(C2), codelist(C2,X).                          % Put spaces (32) arround the ";" (59) and continue to build the list.
codelist(63,[32,63,32|X]) :- get0(C2), codelist(C2,X).                          % Put spaces (32) arround the "?" (63) and continue to build the list.
codelist(C,[C|X])         :- get0(C2), codelist(C2,X).                          % Continue to build the list of codes with the next character code.

% The rest is in dcg syntax:

wordlist([X|Y])          --> word(X), whitespaces, wordlist(Y).                 % A wordlist could be bild of one word, whitespaces and an other wordlist.
wordlist([X])            --> whitespaces, wordlist(X).                          % A wordlist could be bild of whitespaces and the same wordlist.
wordlist([X])            --> word(X).                                           % A wordlist could be bild of one word.
wordlist([X])            --> word(X), whitespaces.                              % A wordlist could be bild of one word and whitespaces character.

word(W)                  --> charlist(X), {name(W,X)}.                          % Build a word from a list of character codes. 
                                                                                %  The prolog built-in predicate "name" represent a list of character codes as Atomic.
charlist([X|Y])          --> chr(X), charlist(Y).                               % A list of characters could be bild of one character and and an other list of characters.
charlist([X])            --> chr(X).                                            % A list of characters could be bild of one character.

chr(X)                   --> [X],{X>=33}.                                       % A useful character code for words is greater than or equal 33.  

whitespaces              --> whitespace, whitespaces.                           % A whitspace could be one and omore whitspaces.
whitespaces              --> whitespace.                                        % A whitspace could be one whitspace

whitespace               --> [X], {X<33}.                                       % A useful character code for whitespaces is less than 33. 