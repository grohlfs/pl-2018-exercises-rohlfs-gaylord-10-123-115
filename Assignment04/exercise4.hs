
--mod :: Int -> Int -> Int  
factors n = [x | x <- [1..n-1], mod n x == 0 ]   	-- factors :: Integral a => a -> [a]
isPerfect n = sum (factors n) == n 					-- isPerfect :: Integral a => a -> Bool



--insert n [] = [n]									-- insert :: a1 -> [a2] -> [a1]
--insert 0 n l = n:l								-- insert :: (Eq a1, Num a1) => a1 -> a2 -> [a2] -> [a2]
insert i n (x:xs) = x : insert (i-1) n xs			-- insert :: Num t1 => t1 -> t2 -> [a] -> [a]



f1 f x												-- f1 :: (Ord t, Num t) => (t -> t) -> t -> [t]
	| f x < 0 = []
	| otherwise = x : (f1 f (f x))
	
	

deleteRepetitions l = delete l
  where
    delete [li] = [li]
    delete (startli:midli:endli)
      | startli == midli  = delete (startli:endli)
      | otherwise = startli : delete (midli:endli)