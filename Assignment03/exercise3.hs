-- Comments

{- Multi Comments

-}

-- Int -2^63 2^63
-- Integer as big as your memory can hold (unbound)
-- Double for floats eg bigFloat 11 floating points
-- Char 'single quotes'
-- Tuple

-- mfac n = map fac n
-- sqr x = x*x
-- map sqr [1..10]
-- msqr l = map sqr l
import Test.HUnit



--always9 = 9 :: Int

--sumOfNums = sum[1..1000]
--addEx = 5 + 4
--modEx = mod 4 5  -- prefix
--modEx2 = 4 `mod` 5 -- infix
--sqrtOf9 = sqrt (fromIntegral always9)


-- Exercise 1

-- [x*x | x <- [1..10]]
-- factorial n = product [1..n]
firstNCatalan n = map catalan [0..n]
fac n | n == 0 = 1
      | n >= 1 = n * fac (n-1)
catalan n = (fac (2*n)) / ((fac (n+1)) * (fac n))

-- Exercise 2
perfectNumbers n m = [x | x <- [n..m-1], sumfac x == x]
sumfac n = sum [y | y <- [1..n], n `mod` y == 0, y /= n]

-- Exercise 3
-- haskell wiki: how_to_work _on_lists
insert i n l = let(lower,upper) = splitAt i l in lower ++ [n] ++ upper 

-- Exercise 4
indexes n l = [x | x <- [0..((length l)-1)], l!!x == n]

-- TESTS
tests = TestList [
  -- exercise 1
  (TestCase (assertEqual "not correct" [1, 1] (firstNCatalan 1 ))),
  (TestCase (assertEqual "not correct" [1, 1, 2, 5, 14, 42, 132] (firstNCatalan 6 ))),

  -- exercise 2
  (TestCase (assertEqual "not correct" [6]     (perfectNumbers 1 28))),
  (TestCase (assertEqual "not correct" [6, 28] (perfectNumbers 1 29 ))),

  -- exercise 3
  (TestCase (assertEqual "not correct" [1, 2, 3, 4, 5] (insert 0 1 [2..5]))),
  (TestCase (assertEqual "not correct" [2, 3, 4, 5, 1] (insert 7 1 [2..5]))),

  -- exercise 4
  (TestCase (assertEqual "not correct" [] (indexes 1 [0,0,0]))),
  (TestCase (assertEqual "not correct" [0, 5] (indexes 6 [6, 3, 4, 5, 1, 6, 5]))),

  -- default check
  (TestCase (assertEqual "" True  True ))]

run = do runTestTT tests
main = run